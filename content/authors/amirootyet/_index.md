---
# Display name
title: Pranshu Bajpai

# Username (this should match the folder name)
authors:
- amirootyet

# Is this the primary user of the site?
superuser: true

# Role/position
role: Security Architect

# Organizations/Affiliations
organizations:
- name: Motorola Solutions
  url: "https://www.motorolasolutions.com/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Systems Security
- Cloud Security
- Malware Analysis
- Digital Forensics
- Cybercrimes
- Data Science

education:
  courses:
  - course: PhD, Computer Science
    institution: Michigan State University
    year: 2020
  - course: MS, Information Security
    institution: Indian Institute of Information Technology
    year: 2014
  - course: MBA, Marketing
    institution: Prestige Institution of Management and Research
    year: 2012

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:bajpai.pranshu@gmail.com".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/AmIRootYet
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=TypMAQwAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/amirootyet
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
#- Researchers
#- Visitors
---

Pranshu Bajpai has research interests in systems security, malware, digital forensics, and cybercrimes. He has authored several papers for reputed magazines and journals including IEEE, Elsevier, ACM, and ISACA. His work has been featured in various media outlets including Scientific American, The Conversation, Salon, Business Standard, Michigan Radio, GCN, GovTech, and others. He is an active speaker at conferences and has spoken at APWG eCrime, DEFCON, GrrCon, Bsides, ToorCon, and many others. He obtained his Ph.D. in Computer Science from Michigan State University and MS in Information Security from Indian Institute of Information Technology.
